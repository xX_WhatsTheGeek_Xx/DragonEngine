#include "mesh_manager.h"
#include <map>
#include "logger.h"
#include "mesh.h"

map<string, Mesh> _meshs;

MeshManagerClass MeshManager;

void MeshManagerClass::AddMesh(Mesh mesh, string id) {
	_meshs.insert(pair<string, Mesh>(id, mesh));
}

Mesh* MeshManagerClass::Get(string id) {
	if (_meshs.find(id) == _meshs.end()) {
		Logger.Panic("Attempting to get a non-existant mesh");
	}
	return &_meshs[id];
}