#include <iostream>
#include "logger.h"
#include "display.h"
#include <GL/glew.h>
#include "mesh.h"
#include "shader.h"
#include "texture.h"
#include "transform.h"
#include "camera.h"
#include "SDL2/SDL.h"
#include "entity.h"
#include "texture_manager.h"
#include "shader_manager.h"
#include "mesh_manager.h"
#include "entity_renderer.h"
#include <map>
#include <fstream>
#include <streambuf>
#include "json.h"
#include <Windows.h>

using json = nlohmann::json;

#undef main

string resources = "";

map<string, string> textureList;
map<string, string> shaderList;
map<string, string> meshList;
vector<Entity> entities;

static string readFile(string path) {
	ifstream file;
	string out;
	string line;
	file.open(path);
	if (!file.is_open()) {
		return "";
	}
	while (file.good()) {
		getline(file, line);
		out += line + "\n";
	}
	return out;
}

int main() {
	Logger.Log("DragonEngine v2.1 - Rawr XD\n");

	Logger.Log("Loading configuration...");

	TCHAR szEXEPath[2048];
	char actualpath[2048];
	GetModuleFileName(NULL, szEXEPath, 2048);

	string execPath = "";
	for (int j = 0; szEXEPath[j] != 0; j++)
	{
		actualpath[j] = szEXEPath[j];
		if (actualpath[j] == '\\') {
			actualpath[j] = '/';
		}
		execPath += actualpath[j];
	}
	int lastPos = 0;
	for (int i = 0; i < execPath.length(); i++) {
		if (execPath[i] == '/') {
			lastPos = i;
		}
	}
	execPath = execPath.substr(0, lastPos + 1);

	string conf = readFile(execPath + "config.json");
	if (conf == "") {
		Logger.Failed();
		Logger.Panic("Config file not found...");
	}
	auto j3 = json::parse(conf);
	string s = j3["res_dir"];
	resources = s;
	Logger.Ok();
	
	Display display(1300, 768, "DragonEngine v2.0");
	
	Logger.Log("Loading shaders...\n");

	shaderList["default"] = "shaders/default";
	shaderList["monochrome"] = "shaders/monochrome";

	for (std::map<string, string>::iterator it = shaderList.begin(); it != shaderList.end(); ++it) {
		Shader shader(resources + it->second + "_vertex.glsl", resources + it->second + "_fragment.glsl", it->first);
		ShaderManager.AddShader(shader, it->first);
	}
	
	Logger.Log("Loading textures...\n");

	textureList["bricks"] = "textures/bricks.png";
	textureList["stall"] = "textures/stall_alt.png";
	textureList["stop"] = "textures/stop.png";
	textureList["pfp"] = "textures/pfp.png";
	textureList["tiles"] = "textures/tiles.png";
	textureList["metal"] = "textures/metal.png";
	textureList["aluminum"] = "textures/aluminum.png";

	for (std::map<string, string>::iterator it = textureList.begin(); it != textureList.end(); ++it) {
		Texture texture(resources + it->second);
		Logger.Log("Loading '" + it->first + "'...");
		if (texture.Width == -1) {
			Logger.Failed();
			Logger.Panic("Texture not found :(");
		}
		TextureManager.AddTexture(texture, it->first);
		Logger.Ok();
	}

	Camera camera(glm::vec3(0.0f, 0.0f, -4.0f), glm::vec3(0.0f, 90.0f, 0.0f), 70, (float)display.Width / (float)display.Height, 0.1f, 100.0f);

	Logger.Log("Loading mesged...\n");

	meshList["cube"] = "models/cube.obj";
	meshList["roundedcube"] = "models/roundedcube.obj";
	meshList["teapot"] = "models/teapot.obj";
	meshList["monkey"] = "models/monkey.obj";
	meshList["sphere"] = "models/sphere.obj";

	for (std::map<string, string>::iterator it = meshList.begin(); it != meshList.end(); ++it) {
		Mesh mesh(resources + it->second);
		Logger.Log("Loading '" + it->first + "'...");
		MeshManager.AddMesh(mesh, it->first);
		Logger.Ok();
	}


	const int _width = 50;
	const int _depth = 50;

	/*for (int x = 0; x < _width; x++) {
		for (int z = 0; z < _depth; z++) {
			EntityRenderer.Entities.push_back(Entity(glm::vec3(1.0f * x, 0.0f, 1.0f * z),
				glm::vec3(0.0f, 0.0f, 0.0f),
				glm::vec3(1.0f, 1.0f, 1.0f),
				"default",
				"tiles",
				"cube"));
		}
	}*/

	EntityRenderer.Entities.push_back(Entity(glm::vec3(0, 0, 0),
			glm::vec3(0.0f, 0.0f, 0.0f),
			glm::vec3(1.0f, 1.0f, 1.0f),
			"default",
			"tiles",
			"teapot"));

	/*EntityRenderer.Entities.push_back(Entity(glm::vec3(27.0f, 5.0f, 25.0f),
		glm::vec3(0.0f, 0.0f, 0.0f),
		glm::vec3(1.0f, 1.0f, 1.0f),
		"default",
		"stop",
		"roundedcube"));

	EntityRenderer.Entities.push_back(Entity(glm::vec3(22.0f, 5.0f, 25.0f),
		glm::vec3(0.0f, 0.0f, 0.0f),
		glm::vec3(1.0f, 1.0f, 1.0f),
		"default",
		"stop",
		"teapot"));*/

	float counter = 0.0f;

	Uint8 lastZoom = 0;
	bool lastFullscreen = false;

	while (!display.isClosed) {
		const Uint8 *state = SDL_GetKeyboardState(NULL);

		if (state[SDL_SCANCODE_W]) {
			camera.Position.z += display.Delta * 0.01f * sin(glm::radians(camera.Rotation.y));
			camera.Position.x += display.Delta * 0.01f * cos(glm::radians(camera.Rotation.y));
		}
		if (state[SDL_SCANCODE_S]) {
			camera.Position.z -= display.Delta * 0.01f * sin(glm::radians(camera.Rotation.y));
			camera.Position.x -= display.Delta * 0.01f * cos(glm::radians(camera.Rotation.y));
		}
		if (state[SDL_SCANCODE_A]) {
			camera.Position.z -= display.Delta * 0.01f * cos(glm::radians(camera.Rotation.y));
			camera.Position.x += display.Delta * 0.01f * sin(glm::radians(camera.Rotation.y));
		}
		if (state[SDL_SCANCODE_D]) {
			camera.Position.z += display.Delta * 0.01f * cos(glm::radians(camera.Rotation.y));
			camera.Position.x -= display.Delta * 0.01f * sin(glm::radians(camera.Rotation.y));
		}
		if (state[SDL_SCANCODE_SPACE]) {
			camera.Position.y += display.Delta * 0.01f;
		}
		if (state[SDL_SCANCODE_LSHIFT]) {
			camera.Position.y -= display.Delta * 0.01f;
		}
		if (state[SDL_SCANCODE_ESCAPE]) {
			display.~Display();
		}

		if (state[SDL_SCANCODE_R] != lastZoom) {
			lastZoom = state[SDL_SCANCODE_R];
			if (state[SDL_SCANCODE_R]) {
				camera.SetFov(20);
			}
			else {
				camera.SetFov(70);
			}
		}

		if (state[SDL_SCANCODE_F10]) {
			EntityRenderer.Entities.at(EntityRenderer.Entities.size() - 1).SetShader("monochrome");
		}
		else {
			EntityRenderer.Entities.at(EntityRenderer.Entities.size() - 1).SetShader("default");
		}

		

		if (state[SDL_SCANCODE_F11]) {
			lastFullscreen = !lastFullscreen;
			display.SetFullscreen(lastFullscreen);
			printf("%d - %d\n", display.Width, display.Height);
			camera.SetAspectRatio((float)display.Width / (float)display.Height);
		}

		SDL_Event e;
		while (SDL_PollEvent(&e)) {
			if (e.type == SDL_QUIT) {
				Logger.Log("Exiting...");
				display.~Display();
			}
			switch (e.type) {
			case SDL_MOUSEMOTION:
				int mouseX = e.motion.xrel;
				int mouseY = e.motion.yrel;
				camera.Rotation.y += mouseX * 0.5f;
				camera.Rotation.x += mouseY * -0.5f;
			}
		}

		display.Clear(0.15f, 0.15f, 0.15f, 1.0f);

		EntityRenderer.Render(camera);

		display.Update();
		counter += 0.01f;
	}
	
	display.~Display();
	
	return 0;
}