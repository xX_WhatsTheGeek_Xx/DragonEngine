#include "shader_manager.h"
#include <map>
#include "logger.h"
#include "shader.h"

map<string, Shader> _shaders;

ShaderManagerClass ShaderManager;

void ShaderManagerClass::AddShader(Shader shader, string id) {
	_shaders.insert(pair<string, Shader>(id, shader));
}

Shader* ShaderManagerClass::Get(string id) {
	if (_shaders.find(id) == _shaders.end()) {
		Logger.Panic("Attempting to get a non-existant shader");
	}
	return &_shaders[id];
}