#include "entity_renderer.h"

EntityRendererClass EntityRenderer;

EntityRendererClass::EntityRendererClass() {

}

void EntityRendererClass::Render(Camera camera) {
	string lastShader = "";
	string lastTexture = "";
	for (int i = 0; i < Entities.size(); i++) {
		if (lastShader != Entities[i].ShaderID) {
			ShaderManager.Get(Entities[i].ShaderID)->Bind();
		}
		Entities[i].Draw(camera, lastTexture != Entities[i].TextureID);
		if (lastTexture != Entities[i].TextureID) {
			lastTexture = Entities[i].TextureID;
		}
	}
}
EntityRendererClass::~EntityRendererClass() {

}
