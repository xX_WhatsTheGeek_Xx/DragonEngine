#include "texture_manager.h"
#include <map>
#include "logger.h"

map<string, Texture> _textures;

TextureManagerClass TextureManager;

void TextureManagerClass::AddTexture(Texture texture, string id) {
	_textures.insert(pair<string, Texture>(id, texture));
}

Texture* TextureManagerClass::Get(string id) {
	if (_textures.find(id) == _textures.end()) {
		Logger.Panic("Attempting to get a non-existant texture");
	}
	return &_textures[id];
}