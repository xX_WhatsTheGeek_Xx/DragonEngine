#include "mesh.h"
#include <vector>
#include "obj_loader.h"

using namespace std;

Mesh::Mesh(Vertex* vertices, unsigned int vertCount, unsigned int* indices, unsigned int indCount) {

	vector<glm::vec3> positions;
	vector<glm::vec2> textureCoords;
	vector<glm::vec3> normals;

	positions.reserve(vertCount);
	textureCoords.reserve(vertCount);
	normals.reserve(vertCount);

	for (int i = 0; i < vertCount; i++) {
		positions.push_back(vertices[i].Position);
		textureCoords.push_back(vertices[i].TextureCoords);
		normals.push_back(vertices[i].Normals);
	}

	init(positions, textureCoords, normals, indices, indCount);
}

Mesh::Mesh(string path) {
	IndexedModel model = OBJModel(path).ToIndexedModel();
	init(model.positions, model.texCoords, model.normals, &model.indices[0], model.indices.size());
}

void Mesh::init(vector<glm::vec3> positions, vector<glm::vec2> textureCoords, vector<glm::vec3> normals, unsigned int* indices, unsigned int indCount) {
	_drawCount = indCount;
	glGenVertexArrays(1, &_vertexArrayObject);
	glBindVertexArray(_vertexArrayObject);

	glGenBuffers(BUFFER_COUNT, _vertexArrayBuffers);

	glBindBuffer(GL_ARRAY_BUFFER, _vertexArrayBuffers[POSITION_VB]);
	glBufferData(GL_ARRAY_BUFFER, positions.size() * sizeof(positions[0]), &positions[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(POSITION_VB);
	glVertexAttribPointer(POSITION_VB, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ARRAY_BUFFER, _vertexArrayBuffers[TEXTURECOORDS_VB]);
	glBufferData(GL_ARRAY_BUFFER, positions.size() * sizeof(textureCoords[0]), &textureCoords[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(TEXTURECOORDS_VB);
	glVertexAttribPointer(TEXTURECOORDS_VB, 2, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ARRAY_BUFFER, _vertexArrayBuffers[NORMAL_VB]);
	glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(normals[0]), &normals[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(NORMAL_VB);
	glVertexAttribPointer(NORMAL_VB, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _vertexArrayBuffers[INDEX_VB]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indCount * sizeof(indices[0]), &indices[0], GL_STATIC_DRAW);


	glBindVertexArray(0);
}

void Mesh::Draw() {
	glBindVertexArray(_vertexArrayObject);
	glDrawElements(GL_TRIANGLES, _drawCount, GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
}

Mesh::~Mesh() {
	//glDeleteVertexArrays(1, &_vertexArrayObject);
}
