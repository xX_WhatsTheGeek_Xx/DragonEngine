#include "logger.h"
#include <ctime>
#include <istream>
#include <Windows.h>
#include <stdlib.h>
#include <string>

#define LOG_WIDTH	50

using namespace std;

HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);

LoggerClass Logger = LoggerClass();

int lastLength = 0;

LoggerClass::LoggerClass() {

}

void LoggerClass::Log(string str) {
	lastLength = str.length();
	time_t now = time(0);
	tm gmtm;
	localtime_s(&gmtm, &now);
	printf("[%02d:%02d:%02d] %s", gmtm.tm_hour, gmtm.tm_min, gmtm.tm_sec, str.c_str());
}

void LoggerClass::Ok() {
	SetConsoleTextAttribute(hConsole, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
	for (int i = 0; i < LOG_WIDTH - lastLength; i++) {
		printf(" ");
	}
	printf("[   ");
	SetConsoleTextAttribute(hConsole, FOREGROUND_GREEN | FOREGROUND_INTENSITY);
	printf("OK");
	SetConsoleTextAttribute(hConsole, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
	printf("   ]\n");
}

void LoggerClass::Warn() {
	SetConsoleTextAttribute(hConsole, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
	for (int i = 0; i < LOG_WIDTH - lastLength; i++) {
		printf(" ");
	}
	printf("[  ");
	SetConsoleTextAttribute(hConsole, FOREGROUND_GREEN | FOREGROUND_RED | FOREGROUND_INTENSITY);
	printf("WARN");
	SetConsoleTextAttribute(hConsole, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
	printf("  ]\n");
}

void LoggerClass::Failed() {
	SetConsoleTextAttribute(hConsole, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
	for (int i = 0; i < LOG_WIDTH - lastLength; i++) {
		printf(" ");
	}
	printf("[ ");
	SetConsoleTextAttribute(hConsole, FOREGROUND_RED | FOREGROUND_INTENSITY);
	printf("FAILED");
	SetConsoleTextAttribute(hConsole, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
	printf(" ]\n");
}

void LoggerClass::Panic(string str) {
	SetConsoleTextAttribute(hConsole, FOREGROUND_RED | FOREGROUND_INTENSITY);
	printf("PANIC: %s\n", str.c_str());
	SetConsoleTextAttribute(hConsole, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
	getchar(); // Wait for exit
	exit(EXIT_FAILURE);
}