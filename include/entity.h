#pragma once
#include <glm/glm.hpp>
#include "shader.h"
#include "mesh.h"
#include "texture.h"
#include "transform.h"
#include "texture_manager.h"
#include "shader_manager.h"
#include "mesh_manager.h"

class Entity {
public:
	inline Entity(glm::vec3 position, glm::vec3 rotation, glm::vec3 scale, string shaderId, string textureId, string meshId) {
		this->Position = position;
		this->Rotation = rotation;
		this->Scale = scale;
		this->ShaderID = shaderId;
		this->TextureID = textureId;
		this->_shader = ShaderManager.Get(shaderId);
		this->_texture = TextureManager.Get(textureId);
		this->_mesh = MeshManager.Get(meshId);
	}

	inline Entity() {
		
	}

	inline void Draw(Camera& camera, bool bindTexture = true) {
		_transform.Rotation = Rotation;
		_transform.Position = Position;
		_transform.Scale = Scale;
		_shader->UpdateUniforms(_transform, camera);
		if (bindTexture) {
			_texture->Bind(0);
		}
		_mesh->Draw();
	}

	inline void SetShader(string shaderId) {
		ShaderID = shaderId;
		_shader = ShaderManager.Get(shaderId);
	}

	inline void SetTexture(string textureId) {
		TextureID = textureId;
		_texture = TextureManager.Get(textureId);
	}

	inline void SetMesh(string meshId) {
		MeshID = meshId;
		_mesh = MeshManager.Get(meshId);
	}

	inline virtual ~Entity() {
		_transform.~Transform();
	}

	glm::vec3 Position;
	glm::vec3 Rotation;
	glm::vec3 Scale;
	string ShaderID;
	string TextureID;
	string MeshID;

private:
	Transform _transform;
	Shader* _shader;
	Texture* _texture;
	Mesh* _mesh;

};
