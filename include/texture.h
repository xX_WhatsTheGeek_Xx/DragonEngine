#pragma once
#include <GL/glew.h>
#include <string>

using namespace std;

class Texture {
public:
	Texture(string path);
	inline Texture() {};
	void Bind(unsigned int unit);
	int Width;
	int Height;
	~Texture();
	Texture(const Texture &texture2) {
		_texture = texture2._texture;
	}

private:
	GLuint _texture;
};

