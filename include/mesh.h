#pragma once
#include <glm/glm.hpp>
#include <GL/glew.h>
#include <string>
#include <vector>

using namespace std;

class Vertex {
public:
	Vertex(const glm::vec3& pos, const glm::vec2 tex, const glm::vec3& norm) {
		this->Position = pos;
		this->TextureCoords = tex;
		this->Normals = norm;
	}
	glm::vec3 Position;
	glm::vec2 TextureCoords;
	glm::vec3 Normals;
};

class Mesh {
public:
	Mesh(Vertex* vertices, unsigned int vertCount, unsigned int* indices, unsigned int indCount);
	Mesh(string path);
	inline Mesh() {};
	void Draw();
	virtual ~Mesh();

private:
	enum {
		POSITION_VB,
		TEXTURECOORDS_VB,
		NORMAL_VB,
		INDEX_VB,
		BUFFER_COUNT
	};
	void init(vector<glm::vec3> positions, vector<glm::vec2> textureCoords, vector<glm::vec3> normals, unsigned int* indices, unsigned int indCount);
	GLuint _vertexArrayObject;
	GLuint _vertexArrayBuffers[BUFFER_COUNT];
	unsigned int _drawCount;
};

