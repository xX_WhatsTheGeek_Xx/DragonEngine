#pragma once
#include <string>
#include "shader.h"

using namespace std;

class ShaderManagerClass {
public:
	void AddShader(Shader shader, string id);
	Shader* Get(string id);
};

extern ShaderManagerClass ShaderManager;