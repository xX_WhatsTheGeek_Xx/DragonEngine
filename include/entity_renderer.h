#pragma once
#include "entity.h"
#include "camera.h"
#include <vector>

class EntityRendererClass {
public:
	EntityRendererClass();
	void Render(Camera camera);
	~EntityRendererClass();
	vector<Entity> Entities;
};

extern EntityRendererClass EntityRenderer;
