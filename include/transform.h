#pragma once
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>

class Transform {
public:
	inline Transform(const glm::vec3& position, const glm::vec3& rotation, const glm::vec3& scale) {
		this->Position = position;
		this->Rotation = rotation;
		this->Scale = scale;
	}

	inline Transform() {
	
	}

	inline glm::mat4 GetModel() {
		glm::mat4 mat = glm::translate(Position);
		mat *= glm::rotate(Rotation.z, glm::vec3(0.0f, 0.0f, 1.0f));
		mat *= glm::rotate(Rotation.y, glm::vec3(0.0f, 1.0f, 0.0f));
		mat *= glm::rotate(Rotation.x, glm::vec3(1.0f, 0.0f, 0.0f));
		mat *= glm::scale(Scale);
		return mat;
	}

	inline ~Transform() {

	}

	glm::vec3 Position;
	glm::vec3 Rotation;
	glm::vec3 Scale;
};

